import java.util.*;

public class Program {
    public static void main(String[] args) {
        ArrayList<String> beers = new ArrayList<String>();

        beers.add("Arboga 10,2");
        beers.add("Fagerhult");
        beers.add("Stockholm festival");
        beers.add("Three Hearts");

        Random random = new Random();

        String randomBeer = beers.get(random.nextInt(beers.size()));

        switch (randomBeer) {
            case "Fagerhult":
                System.out.println("On a hot day I like to drink a " + randomBeer);
                break;
            case "Arboga 10,2":
                System.out.println("On a cold day I like to drink a " + randomBeer);
                break;
            case "Stockholm festival":
                System.out.println("When i'm at a festival I like to drink a " + randomBeer);
                break;
            case "Three Hearts":
                System.out.println("When i'm on a after work I like to drink a " + randomBeer);
                break;
            default:
                break;
        }

    }
}